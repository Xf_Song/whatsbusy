def string_compression(string):
    """
    Implement a string compression using python. For example, aaaabbbccddddddee would become a4b3c2d6e2.
    If the length of the string is not reduced, return the original string.
    :param string: input string
    :return: compressed_str: output compressed string

    Time complexity:
    The string_compression() takes O(n) time, n is the length of origin string. Because it simply iterates the
    characters one by one.
    """
    compressed_str = ""
    if len(string) == 0:
        return compressed_str
    cnt = 1
    compressed_str += string[0]

    for idx in range(len(string) - 1):
        if string[idx] == string[idx + 1]:
            cnt += 1
        else:
            if cnt > 1:
                compressed_str += str(cnt)
            compressed_str += string[idx + 1]
            cnt = 1
    if cnt > 1:
        compressed_str += str(cnt)
    return compressed_str


class Digraph:
    """
    Explain time complexity of the identify_router function written.

    The Digraph directed graph data structure is implemented by storing the number of nodes, directed edges and the
    number of (inbound, outbound) links for each node. It also maintains a max_num for the max number of connections so
    far, and ret_node for the desired nodes with the most number of connections.

    We can add edge dynamically by calling the add_edge method. The add_edge will add the edge and increment the
    self.links for both nodes. Everytime it add an edge, it will also update the max_num and ret_node.

    The most_connection_node method will return the ret_node as nodes with the most number of connections.


    The time complexity for add_edge() is O(1) and most_connection_node() is O(1).
    The construction of the Digraph is O(n).
    """

    def __init__(self, nums_node=6):
        self.nums_node = nums_node
        self.edges = {}
        self.links = [0] * nums_node
        self.max_num = 0
        self.ret_node = []

    def add_edge(self, v, w):
        self.edges.get(v, []).append(w)
        self.links[v - 1] += 1
        self.links[w - 1] += 1

        if self.links[v - 1] > self.max_num:
            self.max_num = self.links[v - 1]
            self.ret_node = [v]
        elif self.links[v - 1] == self.max_num:
            self.ret_node.append(v)

        if self.links[w - 1] > self.max_num:
            self.max_num = self.links[w - 1]
            self.ret_node = [w]
        elif self.links[w - 1] == self.max_num:
            self.ret_node.append(w)

    def most_connection_node(self):
        return self.ret_node


def network_failure_point(network):
    """
    Implement a identify_router function that accepts an input graph of nodes representing the total
    network and identifies the node with the most number of connections.
    Return the label of the node.
    :param network: a list of Node representing nodes
    :return:the label of the node.
    """
    return network.most_connection_node()


if __name__ == "__main__":
    print(string_compression("bbcceeee"))
    print(string_compression("aaabbbcccaaa"))
    print(string_compression("a"))


    network = Digraph(6)
    network.add_edge(2, 4)
    network.add_edge(4, 6)
    network.add_edge(6, 2)
    network.add_edge(2, 5)
    network.add_edge(5, 6)

    # network.add_edge(1, 3)
    # network.add_edge(3, 5)
    # network.add_edge(5, 6)
    # network.add_edge(6, 4)
    # network.add_edge(4, 5)
    # network.add_edge(5, 2)
    # network.add_edge(2, 6)

    print(network_failure_point(network))
