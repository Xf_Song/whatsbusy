from django.db import models
from django import forms


# Create your models here.
class User(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    email = models.CharField(max_length=200)

    def get_password(self):
        return self.password

    class Meta:
        # ordering = ["-c_time"]
        verbose_name = "用户"
        verbose_name_plural = "用户"
