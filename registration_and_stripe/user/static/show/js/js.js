﻿
$(function () {
echarts_1();
echarts_2();
echarts_4();
echarts_31();
echarts_32();
echarts_33();
echarts_5();
echarts_6();
//按照升序排列
function up(x,y){
    return x.number-y.number
}
//按照降序排列
function down(x,y){
    return y.number-x.number
}
//按照升序排列
function up_dead(x,y){
    return x.dead-y.dead
}
//按照降序排列
function down_dead(x,y){
    return y.dead-x.dead
}

$.ajax({
    //请求方式
    type: "POST",
    //请求的媒体类型
    // contentType: "json;charset=UTF-8",
    //请求地址
    url: "home_number",
    //请求成功
    success: function (result) {
        result = JSON.parse(result);
        $(china_confirm_number).text(result["confirm"]);
        $(china_add_confirm_number).text(result["addconfirm"]);

        console.log(result["confirm"]);
        console.log(result["addconfirm"]);
        },
        //请求失败，包含具体的错误信息
        error : function(e){
            console.log(e.status);
            console.log(e.responseText);
        }
});


function echarts_1() {
    $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url : "china_city_confirm_num_rank",
            //请求成功
            success : function(result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                result = JSON.parse(result);
                var x_data = [];
                var y_data = [];

                for (var i = 0; i < 10; i++) {
                    for(var key in result[i]){
                        x_data.push(key);
                        y_data.push(result[i][key]);
                    }
                }
                console.log(x_data);
                console.log(y_data);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart1'));

                option = {
                    //  backgroundColor: '#00265f',
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                },
                grid: {
                    left: '0%',
                    top:'10px',
                    right: '0%',
                    bottom: '4%',
                   containLabel: true
                },
                xAxis: [{
                    type: 'category',
                        data: x_data,
                    axisLine: {
                        show: true,
                     lineStyle: {
                            color: "rgba(255,255,255,.1)",
                            width: 1,
                            type: "solid"
                        },
                    },

                    axisTick: {
                        show: false,
                    },
                    axisLabel:  {
                            interval: 0,
                           // rotate:50,
                            show: true,
                            splitNumber: 15,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                }],
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                       //formatter: '{value} %'
                        show:true,
                         textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                    },
                    axisTick: {
                        show: false,
                    },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: "rgba(255,255,255,.1	)",
                            width: 1,
                            type: "solid"
                        },
                    },
                    splitLine: {
                        lineStyle: {
                           color: "rgba(255,255,255,.1)",
                        }
                    }
                }],
                series: [
                    {
                    type: 'bar',
                    data: y_data,
                    barWidth:'35%', //柱子宽度
                   // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color:'#2f89cf',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }

                ]
            };

                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);
                    window.addEventListener("resize",function(){
                        myChart.resize();
                    });
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
function echarts_2() {
    $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url : "china_city_add_confirm_num_rank",
            //请求成功
            success : function(result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                result = JSON.parse(result);
                result = result.sort(down);
                var x_data = [];
                var y_data = [];

                for (var i = 0; i < 10; i++) {
                    x_data.push(result[i]["city"]);
                    y_data.push(result[i]["number"]);
                }
                console.log(x_data);
                console.log(y_data);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart2'));

                option = {
                //  backgroundColor: '#00265f',
                tooltip: {
                    trigger: 'axis',
                    axisPointer: { type: 'shadow'}
                },
                grid: {
                    left: '0%',
                    top:'10px',
                    right: '0%',
                    bottom: '4%',
                   containLabel: true
                },
                xAxis: [{
                    type: 'category',
                        data: x_data,
                    axisLine: {
                        show: true,
                     lineStyle: {
                            color: "rgba(255,255,255,.1)",
                            width: 1,
                            type: "solid"
                        },
                    },

                    axisTick: {
                        show: false,
                    },
                    axisLabel:  {
                            interval: 0,
                           // rotate:50,
                            show: true,
                            splitNumber: 15,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                }],
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                       //formatter: '{value} %'
                        show:true,
                         textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                    },
                    axisTick: {
                        show: false,
                    },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: "rgba(255,255,255,.1	)",
                            width: 1,
                            type: "solid"
                        },
                    },
                    splitLine: {
                        lineStyle: {
                           color: "rgba(255,255,255,.1)",
                        }
                    }
                }],
                series: [
                    {

                    type: 'line',
                    data: y_data,
                    barWidth:'35%', //柱子宽度
                   // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color:'#27d08a',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }

                ]
            };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize",function(){
                    myChart.resize();
                });
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
}

function echarts_4() {
    $.ajax({
            //请求方式
            type : "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url : "china_city_confirm_num_rank",
            //请求成功
            success : function(result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                result = JSON.parse(result);
                var x_data = [];
                var y_data = [];

                for (var i = 0; i < 10; i++) {
                    for(var key in result[i]){
                        x_data.push(key);
                        y_data.push(result[i][key]);
                    }
                }
                console.log("..echarts4..");
                console.log(y_data);
                console.log("..echarts4..");
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart4'));

                option = {
                    //  backgroundColor: '#00265f',
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                },
                grid: {
                    left: '0%',
                    top:'10px',
                    right: '0%',
                    bottom: '4%',
                   containLabel: true
                },
                xAxis: [{
                    type: 'category',
                        data: x_data,
                    axisLine: {
                        show: true,
                     lineStyle: {
                            color: "rgba(255,255,255,.1)",
                            width: 1,
                            type: "solid"
                        },
                    },

                    axisTick: {
                        show: false,
                    },
                    axisLabel:  {
                            interval: 0,
                           // rotate:50,
                            show: true,
                            splitNumber: 15,
                            textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                        },
                }],
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                       //formatter: '{value} %'
                        show:true,
                         textStyle: {
                                color: "rgba(255,255,255,.6)",
                                fontSize: '12',
                            },
                    },
                    axisTick: {
                        show: false,
                    },
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: "rgba(255,255,255,.1	)",
                            width: 1,
                            type: "solid"
                        },
                    },
                    splitLine: {
                        lineStyle: {
                           color: "rgba(255,255,255,.1)",
                        }
                    }
                }],
                series: [
                    {
                    type: 'line',
                    data: y_data,
                    barWidth:'35%', //柱子宽度
                   // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color:'#2f89cf',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }

                ]
            };

                    // 使用刚指定的配置项和数据显示图表。
                    myChart.setOption(option);
                    window.addEventListener("resize",function(){
                        myChart.resize();
                    });
            },
            //请求失败，包含具体的错误信息
            error : function(e){
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }

function echarts_5() {
        $.ajax({
            //请求方式
            type: "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url: "china_city_heal_num_rank",
            //请求成功
            success: function (result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                result = JSON.parse(result);
                result = result.sort(down);
                var x_data = [];
                var y_data = [];

                for (var i = 0; i < 10; i++) {
                    x_data.push(result[i]["city"]);
                    y_data.push(result[i]["number"]);
                }
                console.log(x_data);
                console.log(y_data);
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart5'));

                option = {
                //  backgroundColor: '#00265f',
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'shadow'
                        }
                    },

                    grid: {
                        left: '0%',
                        top:'10px',
                        right: '0%',
                        bottom: '2%',
                       containLabel: true
                    },
                    xAxis: [{
                        type: 'category',
                            data: x_data,
                        axisLine: {
                            show: true,
                         lineStyle: {
                                color: "rgba(255,255,255,.1)",
                                width: 1,
                                type: "solid"
                            },
                        },

                        axisTick: {
                            show: false,
                        },
                        axisLabel:  {
                                interval: 0,
                               // rotate:50,
                                show: true,
                                splitNumber: 15,
                                textStyle: {
                                    color: "rgba(255,255,255,.6)",
                                    fontSize: '12',
                                },
                            },
                    }],
                    yAxis: [{
                        type: 'value',
                        axisLabel: {
                           //formatter: '{value} %'
                            show:true,
                             textStyle: {
                                    color: "rgba(255,255,255,.6)",
                                    fontSize: '12',
                                },
                        },
                        axisTick: {
                            show: false,
                        },
                        axisLine: {
                            show: true,
                            lineStyle: {
                                color: "rgba(255,255,255,.1	)",
                                width: 1,
                                type: "solid"
                            },
                        },
                        splitLine: {
                            lineStyle: {
                               color: "rgba(255,255,255,.1)",
                            }
                        }
                    }],
                    series: [{
                        type: 'bar',
                        data: y_data,
                        barWidth:'35%', //柱子宽度
                       // barGap: 1, //柱子之间间距
                        itemStyle: {
                            normal: {
                                color:'#2f89cf',
                                opacity: 1,
                                barBorderRadius: 5,
                            }
                        }
                    }
                    ]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize",function(){
                    myChart.resize();
                });

            },
            //请求失败，包含具体的错误信息
            error: function (e) {
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
	
// function echarts_4() {
//     $.ajax({
//         //请求方式
//         type: "POST",
//         //请求的媒体类型
//         // contentType: "json;charset=UTF-8",
//         //请求地址
//         url: "china_dead_heal",
//         //请求成功
//         success: function (result) {
//             // console.log(result);
//             // console.log(JSON.parse(result));
//             result = JSON.parse(result);
//             result = result.sort(down);
//             var x_data = [];
//             var y_data1 = [];
//             var y_data2 = [];
//
//             for (var i = result.length-10; i < result.length; i++) {
//                 x_data.push(result[i]["date"]);
//                 y_data1.push(result[i]["dead"]);
//                 y_data2.push(result[i]["heal"]);
//             }
//             console.log(x_data);
//             console.log(y_data1);
//             // 基于准备好的dom，初始化echarts实例
//             var myChart = echarts.init(document.getElementById('echart4'));
//
//             option = {
//                     tooltip: {
//                     trigger: 'axis',
//                     axisPointer: {
//                         lineStyle: {
//                             color: '#dddc6b'
//                         }
//                     }
//                 },
//                         legend: {
//                 top:'0%',
//                     data:['死亡人数','治愈人数'],
//                             textStyle: {
//                        color: 'rgba(255,255,255,.5)',
//                         fontSize:'12',
//                     }
//                 },
//                 grid: {
//                     left: '10',
//                     top: '30',
//                     right: '10',
//                     bottom: '10',
//                     containLabel: true
//                 },
//
//                 xAxis: [{
//                     type: 'category',
//                     boundaryGap: false,
//                     axisLabel:  {
//                             textStyle: {
//                                 color: "rgba(255,255,255,.6)",
//                                 fontSize:12,
//                             },
//                         },
//                     axisLine: {
//                         lineStyle: {
//                             color: 'rgba(255,255,255,.2)'
//                         }
//
//                     },
//
//                     data: x_data
//
//                 }, {
//
//                     axisPointer: {show: false},
//                     axisLine: {  show: false},
//                     position: 'bottom',
//                     offset: 20,
//
//
//
//                 }],
//
//                 yAxis: [{
//                     type: 'value',
//                     axisTick: {show: false},
//                     axisLine: {
//                         lineStyle: {
//                             color: 'rgba(255,255,255,.1)'
//                         }
//                     },
//                    axisLabel:  {
//                             textStyle: {
//                                 color: "rgba(255,255,255,.6)",
//                                 fontSize:12,
//                             },
//                         },
//
//                     splitLine: {
//                         lineStyle: {
//                              color: 'rgba(255,255,255,.1)'
//                         }
//                     }
//                 }],
//                 series: [
//                     {
//                     name: '死亡人数',
//                     type: 'line',
//                      smooth: true,
//                     symbol: 'circle',
//                     symbolSize: 5,
//                     showSymbol: false,
//                     lineStyle: {
//
//                         normal: {
//                             color: '#0184d5',
//                             width: 2
//                         }
//                     },
//                     areaStyle: {
//                         normal: {
//                             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                                 offset: 0,
//                                 color: 'rgba(1, 132, 213, 0.4)'
//                             }, {
//                                 offset: 0.8,
//                                 color: 'rgba(1, 132, 213, 0.1)'
//                             }], false),
//                             shadowColor: 'rgba(0, 0, 0, 0.1)',
//                         }
//                     },
//                         itemStyle: {
//                         normal: {
//                             color: '#0184d5',
//                             borderColor: 'rgba(221, 220, 107, .1)',
//                             borderWidth: 12
//                         }
//                     },
//                     data: y_data1
//
//                 },
//             {
//                     name: '治愈人数',
//                     type: 'line',
//                     smooth: true,
//                     symbol: 'circle',
//                     symbolSize: 5,
//                     showSymbol: false,
//                     lineStyle: {
//
//                         normal: {
//                             color: '#00d887',
//                             width: 2
//                         }
//                     },
//                     areaStyle: {
//                         normal: {
//                             color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
//                                 offset: 0,
//                                 color: 'rgba(0, 216, 135, 0.4)'
//                             }, {
//                                 offset: 0.8,
//                                 color: 'rgba(0, 216, 135, 0.1)'
//                             }], false),
//                             shadowColor: 'rgba(0, 0, 0, 0.1)',
//                         }
//                     },
//                         itemStyle: {
//                         normal: {
//                             color: '#00d887',
//                             borderColor: 'rgba(221, 220, 107, .1)',
//                             borderWidth: 12
//                         }
//                     },
//                     data: y_data2
//
//                 },
//
//                      ]
//
//             };
//
//             // 使用刚指定的配置项和数据显示图表。
//             myChart.setOption(option);
//             window.addEventListener("resize",function(){
//                 myChart.resize();
//             });
//         },
//         //请求失败，包含具体的错误信息
//         error: function (e) {
//             console.log(e.status);
//             console.log(e.responseText);
//         }
//     });
//
// }
function echarts_6() {
        $.ajax({
            //请求方式
            type: "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url: "china_city_dead_num_rank",
            //请求成功
            success: function (result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                result = JSON.parse(result);
                result = result.sort(down_dead);
                var x_data = [];
                var y_data = [];

                for (var i = 0; i < 5; i++) {
                    x_data.push(result[i]["city"]);
                    y_data.push(result[i]["dead"]);
                }
                console.log(x_data);
                console.log(y_data);


                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('echart6'));

                var dataStyle = {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        },
                        //shadowBlur: 40,
                        //shadowColor: 'rgba(40, 40, 40, 1)',
                    }
                };
                var placeHolderStyle = {
                    normal: {
                        color: 'rgba(255,255,255,.05)',
                        label: {show: false,},
                        labelLine: {show: false}
                    },
                    emphasis: {
                        color: 'rgba(0,0,0,0)'
                    }
                };
                option = {
                    color: ['#0f63d6', '#0f78d6', '#0f8cd6', '#0fa0d6', '#0fb4d6'],
                    tooltip: {
                        show: true,
                        formatter: "{a} : {c} "
                    },
                    legend: {
                        itemWidth: 10,
                        itemHeight: 10,
                        itemGap: 12,
                        bottom: '3%',

                        data: x_data,
                        textStyle: {
                                    color: 'rgba(255,255,255,.6)',
                                }
                    },

                    series: [
                        {
                        name: x_data[0],
                        type: 'pie',
                        clockWise: false,
                        center: ['50%', '42%'],
                        radius: ['59%', '70%'],
                        itemStyle: dataStyle,
                        hoverAnimation: false,
                        data: [{
                            value: y_data[0],
                            name: '01'
                        }, {
                            value: 100-y_data[0],
                            name: 'invisible',
                            tooltip: {show: false},
                            itemStyle: placeHolderStyle
                        }]
                    },
                        {
                        name: x_data[1],
                        type: 'pie',
                        clockWise: false,
                        center: ['50%', '42%'],
                        radius: ['49%', '60%'],
                        itemStyle: dataStyle,
                        hoverAnimation: false,
                        data: [{
                            value: y_data[1],
                            name: '02'
                        }, {
                            value: 100-y_data[1],
                            name: 'invisible',
                            tooltip: {show: false},
                            itemStyle: placeHolderStyle
                        }]
                    },
                        {
                        name: x_data[2],
                        type: 'pie',
                        clockWise: false,
                        hoverAnimation: false,
                        center: ['50%', '42%'],
                        radius: ['39%', '50%'],
                        itemStyle: dataStyle,
                        data: [{
                            value: y_data[2],
                            name: '03'
                        }, {
                            value: 100-y_data[2],
                            name: 'invisible',
                            tooltip: {show: false},
                            itemStyle: placeHolderStyle
                        }]
                    },
                        {
                        name: x_data[3],
                        type: 'pie',
                        clockWise: false,
                        hoverAnimation: false,
                        center: ['50%', '42%'],
                        radius: ['29%', '40%'],
                        itemStyle: dataStyle,
                        data: [{
                            value: y_data[3],
                            name: '04'
                        }, {
                            value: 100-y_data[3],
                            name: 'invisible',
                            tooltip: {show: false},
                            itemStyle: placeHolderStyle
                        }]
                    },
                        {
                        name: x_data[4],
                        type: 'pie',
                        clockWise: false,
                        hoverAnimation: false,
                        center: ['50%', '42%'],
                        radius: ['20%', '30%'],
                        itemStyle: dataStyle,
                        data: [{
                            value: y_data[4],
                            name: '05'
                        }, {
                            value: 100-y_data[0],
                            name: 'invisible',
                            tooltip: {show: false},
                            itemStyle: placeHolderStyle
                        }]
                    }, ]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize",function(){
                    myChart.resize();
                });
            },
            //请求失败，包含具体的错误信息
            error: function (e) {
                console.log(e.status);
                console.log(e.responseText);
            }
        });
}
function echarts_31() {
        $.ajax({
            //请求方式
            type: "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url: "guangdong_info",
            //请求成功
            success: function (result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                // result = JSON.parse(result);
                // result = result.sort(down_dead);
                var x_data = ["确诊人数","死亡人数","治愈人数"];
                var y_data = [result["confirm"],result["dead"],result["heal"]];
                var sum = result["confirm"] + result["dead"] + result["heal"]
                console.log("==================");
                console.log(x_data);
                console.log(y_data);
                console.log("==================");

                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('fb1'));
                option = {
                    title: [{
                        text: '广东疫情人数分布',
                        left: 'center',
                        textStyle: {
                            color: '#fff',
                            fontSize:'16'
                        }
                }],
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)",
                    position:function(p){   //其中p为当前鼠标的位置
                        return [p[0] + 10, p[1] - 10];
                    }
                },
                legend: {

                top:'70%',
                   itemWidth: 10,
                    itemHeight: 10,
                    data: x_data,
                    textStyle: {
                    color: 'rgba(255,255,255,.5)',
                    fontSize:'12',
                    }
                },
                series: [
                    {
                        name:'广东疫情人数分布',
                        type:'pie',
                        center: ['50%', '42%'],
                        radius: ['40%', '60%'],
                              color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab','#06b4ab','#06c8ab','#06dcab','#06f0ab'],
                        label: {show:false},
                        labelLine: {show:false},
                        data:[
                            {value:y_data[0], name:x_data[0]},
                            {value:4, name:x_data[1]},
                            {value:2, name:x_data[2]}
                        ]
                    }
                ]
            };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize",function(){
                    myChart.resize();
                });
            },
            //请求失败，包含具体的错误信息
            error: function (e) {
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
function echarts_32() {
        $.ajax({
            //请求方式
            type: "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url: "hubei_info",
            //请求成功
            success: function (result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                // result = JSON.parse(result);
                // result = result.sort(down_dead);
                var x_data = ["确诊人数","死亡人数","治愈人数"];
                var y_data = [result["confirm"],result["dead"],result["heal"]];
                var sum = result["confirm"] + result["dead"] + result["heal"]
                console.log("==================");
                console.log(x_data);
                console.log(y_data);
                console.log("==================");

                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('fb2'));
                option = {

                    title: [{
                    text: '湖北疫情人数分布',
                    left: 'center',
                    textStyle: {
                        color: '#fff',
                        fontSize:'16'
                    }

                }],
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)",
                    position:function(p){   //其中p为当前鼠标的位置
                        return [p[0] + 10, p[1] - 10];
                    }
                },
                legend: {
                    top:'70%',
                       itemWidth: 10,
                        itemHeight: 10,
                        data:x_data,
                                textStyle: {
                           color: 'rgba(255,255,255,.5)',
                            fontSize:'12',
                        }
                    },
                    series: [
                        {
                            name:'湖北疫情人数分布',
                            type:'pie',
                            center: ['50%', '42%'],
                            radius: ['40%', '60%'],
                            color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab','#06b4ab','#06c8ab','#06dcab','#06f0ab'],
                            label: {show:false},
                            labelLine: {show:false},
                            data:[
                                {value:y_data[0], name:x_data[0]},
                                {value:y_data[1], name:x_data[1]},
                                {value:y_data[2], name:x_data[2]}
                            ]
                        }
                    ]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize",function(){
                    myChart.resize();
                });
        },
            //请求失败，包含具体的错误信息
            error: function (e) {
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
function echarts_33() {
        $.ajax({
            //请求方式
            type: "POST",
            //请求的媒体类型
            // contentType: "json;charset=UTF-8",
            //请求地址
            url: "zhejiang_info",
            //请求成功
            success: function (result) {
                // console.log(result);
                // console.log(JSON.parse(result));
                // result = JSON.parse(result);
                // result = result.sort(down_dead);
                var x_data = ["确诊人数","死亡人数","治愈人数"];
                var y_data = [result["confirm"],result["dead"],result["heal"]];
                var sum = result["confirm"] + result["dead"] + result["heal"]
                console.log("==================");
                console.log(x_data);
                console.log(y_data);
                console.log("==================");

                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('fb3'));
                    option = {
                    title: [{
                    text: '浙江疫情人数分布',
                    left: 'center',
                    textStyle: {
                        color: '#fff',
                        fontSize:'16'
                    }

                }],
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)",
                    position:function(p){   //其中p为当前鼠标的位置
                        return [p[0] + 10, p[1] - 10];
                    }
                },
                legend: {
                        top:'70%',
                        itemWidth: 10,
                        itemHeight: 10,
                        data:x_data,
                        textStyle: {
                        color: 'rgba(255,255,255,.5)',
                        fontSize:'12',
                        }
                    },
                    series: [
                        {
                            name:'浙江疫情人数分布',
                            type:'pie',
                            center: ['50%', '42%'],
                            radius: ['40%', '60%'],
                                   color: ['#065aab', '#066eab', '#0682ab', '#0696ab', '#06a0ab','#06b4ab','#06c8ab','#06dcab','#06f0ab'],
                            label: {show:false},
                            labelLine: {show:false},
                            data:[
                                {value:y_data[0], name:x_data[0]},
                                {value:y_data[1], name:x_data[1]},
                                {value:y_data[2], name:x_data[2]}
                            ]
                        }
                    ]
                };

                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);
                window.addEventListener("resize",function(){
                    myChart.resize();
                });
            },
            //请求失败，包含具体的错误信息
            error: function (e) {
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
})



		
		
		


		









