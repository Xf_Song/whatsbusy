from django.shortcuts import render
from django.shortcuts import redirect
from user.models import User
from django.shortcuts import HttpResponse
from django.http import JsonResponse, HttpResponseRedirect
import json
from django.core.serializers import serialize
from django.core import serializers


def to_login(request):
    return render(request, 'django_registration/login.html')


def login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        if username and password:  # 确保用户名和密码都不为空
            username = username.strip()
            try:
                user = User.objects.get(username=username)
                print(user)
            except Exception as e:
                print(e)
                print("登录失败")
                return render(request, 'django_registration/login.html')
                # return HttpResponse(json.dumps(data))
            if user.get_password() == password:
                request.session['username'] = user.username
                return render(request, 'django_registration/main.html', {"user": user})
    return render(request, 'django_registration/main.html')


def update_password(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        print(username)
        print(password)
        User.objects.filter(username=username).first().update(password=password)
    return JsonResponse({"status": "success"}, safe=False)


def register(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        user_obj = User.objects.create(username=username, password=password, email=email)
        print(user_obj)
        return render(request, 'django_registration/login.html')
    return render(request, 'django_registration/registration_form.html')


def to_reg(request):
    return render(request, 'django_registration/registration_form.html')


def to_main(request):
    return render(request, 'django_registration/main.html')
